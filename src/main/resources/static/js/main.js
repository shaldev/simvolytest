$(document).ready(function () {
    const group = $("select#group");

    $.ajax({
        type: "get",
        url: "/groups",
        dataType: "json",
        success: function (g) {
            g.forEach(function (item) {
                group.append("<option value='" + item.id + "'>" + item.name + "</option>");
            });
        }
    });

    $('button').click(function () {
        const email = $("input[name='email']"),
            name = $("input[name='name']"),
            size = $("select[name='size']");

        if (name.val() === "" || email.val() === "") {
            displayMessage("WARNING", "Fill in all required fields");
            return;
        }

        $.ajax({
            type: "post",
            data: {
                email: email.val(),
                name: name.val(),
                size: size.val(),
                group: group.val()
            },
            url: "/submit",
            dataType: "json",
            success: function (e) {
                displayMessage(e.type, e.message);

                email.val("");
                name.val("");
            }
        });
    });
});

function displayMessage(type, message) {
    let container = $("#message-container");
    container.toggleClass("hide");
    container.empty();

    if (type === "INFO") {
        container.append('<div id="alert" class="alert alert-success" role="alert">' + message + '</div>');
    } else if (type === "ERROR") {
        container.append('<div id="alert" class="alert alert-danger" role="alert">' + message + '</div>');
    } else if (type === "WARNING") {
        container.append('<div id="alert" class="alert alert-warning" role="alert">' + message + '</div>');
    }

    setTimeout(function () {
        container.toggleClass("hide");
    }, 5000);
}