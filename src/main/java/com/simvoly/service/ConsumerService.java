package com.simvoly.service;

import com.simvoly.model.Contact;
import com.simvoly.model.Group;
import com.simvoly.model.Result;
import com.simvoly.model.Subscriber;
import com.simvoly.helpers.Helpers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class ConsumerService {

    private RestTemplate template;
    private Helpers helpers;

    @Autowired
    public ConsumerService(RestTemplate template, Helpers helpers) {
        this.template = template;
        this.helpers = helpers;
    }

    public List<Group> getGroups() {
        HttpEntity<String> entity = helpers.getEntity();

        String url = "http://api.mailerlite.com/api/v2/groups";
        ResponseEntity<Group[]> groups = template.exchange(url, HttpMethod.GET, entity, Group[].class);

        if (groups.getBody() == null) {
            return null;
        }

        return List.of(groups.getBody());
    }

    public Result addSubscriber(String email, String name, int groupId, String size) {

        Map<String, String> fields = new HashMap<>();
        fields.put("size", size);

        Contact newUser = new Contact(email, name, fields);
        HttpEntity<Contact> user = helpers.getEntity(newUser);

        String url = "https://api.mailerlite.com/api/v2/groups/" + groupId + "/subscribers";
        ResponseEntity<Subscriber> subscriber = template.postForEntity(url, user, Subscriber.class);

        if (subscriber.getStatusCode() != HttpStatus.OK) {
            return new Result(Result.Type.ERROR, "API Error");
        }

        return new Result(Result.Type.INFO, "Success");
    }
}
