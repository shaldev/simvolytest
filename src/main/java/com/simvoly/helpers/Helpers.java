package com.simvoly.helpers;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;

@Configuration
public class Helpers {

    @Value("${api.key}")
    private String apiKey;

    private HttpHeaders getHeaders() {
        final HttpHeaders headers = new HttpHeaders();

        headers.set("X-MailerLite-ApiKey", apiKey);

        return headers;
    }

    public <T> HttpEntity<T> getEntity() {
        HttpHeaders headers = getHeaders();

        return new HttpEntity<>(headers);
    }

    public <T> HttpEntity<T> getEntity(T object) {
        HttpHeaders headers = getHeaders();

        return new HttpEntity<>(object, headers);
    }
}
