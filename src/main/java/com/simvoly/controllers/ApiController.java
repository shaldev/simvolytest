package com.simvoly.controllers;

import com.simvoly.model.Group;
import com.simvoly.model.Result;
import com.simvoly.service.ConsumerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ApiController {

    private ConsumerService service;

    @Autowired
    public ApiController(ConsumerService consumer) {
        this.service = consumer;
    }

    @RequestMapping("/groups")
    public List<Group> getGroups() {
        return service.getGroups();
    }

    @PostMapping("/submit")
    public Result submit(@RequestParam(required = true) String email,
                         @RequestParam(required = true) String name,
                         @RequestParam(required = true) int group,
                         @RequestParam(required = true) String size) {

        return service.addSubscriber(email, name, group, size);
    }
}
