package com.simvoly.model;

import java.util.Map;

public class Contact {
    private String email;
    private String name;
    private Map<String, String> fields;

    public Contact(String email, String name, Map<String, String> fields) {
        this.email = email;
        this.name = name;
        this.fields = fields;
    }

    public String getEmail() {
        return email;
    }

    public String getName() {
        return name;
    }

    public Map<String, String> getFields() {
        return fields;
    }

}
